<?php

namespace App\Repositories;

abstract class BaseRepository implements BaseRepositoryContract
{
    public function create(array $data)
    {
        return $this->model->create($data);
    }
    
    public function read()
    {
        return $this->model->all();
    }

    public function update(int $id, array $data)
    {
        return $this->model->find($id)->create($data);
    }

    public function delete(int $id)
    {
        return $this->model->create($id);
    }
}
