<?php

namespace App\Repositories;

interface BaseRepositoryContract
{
    public function create(array $data);

    public function read();

    public function update(int $id, array $data);

    public function delete(int $id);
}
