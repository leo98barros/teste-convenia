<?php

namespace App\Repositories\Employee;

use App\Models\Employee;
use App\Repositories\BaseRepositoryContract;

interface EmployeeRepositoryContract extends BaseRepositoryContract
{
    public function updateOrCreate(array $searchBy, array $upsertData): Employee;
}
