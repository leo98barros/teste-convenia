<?php

namespace App\Repositories\Employee;

use App\Models\Employee;
use App\Repositories\BaseRepository;

class EmployeeRepositoryEloquent extends BaseRepository implements EmployeeRepositoryContract
{
    protected Employee $model;

    public function __construct(Employee $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $searchBy
     * @param array $upsertData
     * @return Employee
     */
    public function updateOrCreate(array $searchBy, array $upsertData): Employee
    {
        return $this->model->updateOrCreate(
            $searchBy, $upsertData
        );
    }
}