<?php

namespace App\Imports;

use App\Models\{
    Employee,
    User
};

use App\Notifications\EmployeeImportedNotification;
use App\Repositories\Employee\EmployeeRepositoryContract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Arr;

use Maatwebsite\Excel\Concerns\{
    Importable,
    RegistersEventListeners,
    ToModel,
    WithChunkReading,
    WithEvents,
    WithHeadingRow,
    WithValidation,
};

use Maatwebsite\Excel\Events\AfterImport;

class EmployeeImport implements ToModel, WithChunkReading, WithEvents, WithHeadingRow, WithValidation
{
    use Importable, RegistersEventListeners;

    /**
     * @var EmployeeRepositoryContract 
     */
    private $employeeRepository;

    /**
     * @var User 
     */
    public $user;

    /**
     * @param EmployeeRepositoryContract $employeeRepository
     * @param User $user
     */
    public function __construct(
        EmployeeRepositoryContract $employeeRepository, 
        User $user
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->user = $user;
    }

    /**
     * @param array $row
     *
     * @return Employee|null
     */
    public function model(array $row): ?Employee
    {
        list(
            'searchBy' => $searchBy, 
            'upsertData' => $upsertData
        ) = $this->prepareUpsert($row);

        return $this->employeeRepository->updateOrCreate($searchBy, $upsertData);
    }

    /*
    |--------------------------------------------------------------------------
    | Concerns functions
    |--------------------------------------------------------------------------
    */

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterImport::class => function (AfterImport $event) {
                $this->user->notify(new EmployeeImportedNotification("Employees imported successfully"));
            }
        ];
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 50000;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'string',
            'email' => 'email',
            'document' => 'numeric',
            'city' => 'string',
            'state' => 'string|size:2',
            'start_date' => 'date',
        ];
    }

    /**
     * @return array
     */
    public function customValidationMessages(): array
    {
        return [
            'name.string' => 'name must be a string',
            'email.string' => 'invalid email',
            'document.numeric' => 'name must be a string',
            'city.string' => 'city must be a string',
            'state.string' => 'state must be a string',
            'state.size' => 'state must be a valid UF',
            'start_date.date' => 'invalid date',
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Private functions
    |--------------------------------------------------------------------------
    */

    /**
     * Merge the document row with user id
     * 
     * @param array $row
     * 
     * @return array
     */
    private function mergeUserIdIntoRowData(array $row): array
    {
        return Arr::add($row, 'user_id', $this->user->id);
    }

    /**
     * @return array
     */
    private function prepareUpsert(array $row): array
    {
        $mergedRow = $this->mergeUserIdIntoRowData($row);

        return [
            'searchBy' => Arr::only($mergedRow, ['email', 'document']),
            'upsertData' => Arr::only($mergedRow, ['name', 'city', 'state', 'start_date', 'user_id']),
        ];
    }
}
