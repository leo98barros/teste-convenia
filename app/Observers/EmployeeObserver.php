<?php

namespace App\Observers;

use App\Models\Employee;
use App\Notifications\EmployeeImportedNotification;

class EmployeeObserver
{
    /**
     * Handle the Employee "updated" event.
     *
     * @param Employee  $employee
     * @return void
     */
    public function updated(Employee $employee)
    {
        $message = "Employee $employee->id - $employee->name updated";

        auth()->user()->notify(new EmployeeImportedNotification($message));
    }
}
