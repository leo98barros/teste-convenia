<?php

namespace App\Providers;

use App\Repositories\Employee\{
    EmployeeRepositoryContract,
    EmployeeRepositoryEloquent
};

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            EmployeeRepositoryContract::class,
            EmployeeRepositoryEloquent::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return void
     */
    public function provides()
    {
        return [
            EmployeeRepositoryContract::class,
        ];
    }
}
