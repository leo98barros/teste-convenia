<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employee\StoreOrUpdateRequest;
use App\Imports\EmployeeImport;
use App\Models\Employee;
use App\Repositories\Employee\EmployeeRepositoryContract;

use Exception;

use Illuminate\Http\{
    JsonResponse,
    Request,
    Response
};

use Maatwebsite\Excel\Validators\ValidationException;

class EmployeeController extends Controller
{
    /**
     * @var EmployeeRepositoryContract
     */
    private $employeeRepository;

    /**
     * @param EmployeeRepositoryContract $employeeRepository
     */
    public function __construct(EmployeeRepositoryContract $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * Get all employees of authenticaded user
     * 
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    /**
     * Get a specific employee of authenticaded user
     * 
     * @param Employee $employee
     * 
     * @return JsonResponse
     */
    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    /**
     * Stores or Updates employees of authenticaded user passed by csv
     * 
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function storeOrUpdate(StoreOrUpdateRequest $request): JsonResponse
    {
        try {
            (new EmployeeImport($this->employeeRepository, auth()->user()))
                ->import($request->file->path());
            
            return response()->json([
                'message' => "Import has started, you'll receive an email with the status of the process"
            ]);

        } catch (ValidationException $exception) {
            $failures = $exception->failures();

            foreach ($failures as $key => $failure) {
                return response()->json([
                        'message' => 'NOT ACCEPTABLE: ' . $failure->errors()[$key]
                    ], Response::HTTP_NOT_ACCEPTABLE
                );
            }

        } catch (Exception $exception) {
            return response()->json(
                $exception->getMessage(), 
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Delete a specific employee of authenticaded user
     * 
     * @param Employee $employee
     * 
     * @return JsonResponse
     */
    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }
}
