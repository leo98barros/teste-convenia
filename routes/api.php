<?php

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication Routes
Route::as('auth.')->prefix('auth')->group(function () {
    Route::post('login', [Controllers\AuthController::class, 'login'])->name('login');
});

// Authenticated Routes
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('/teste', function (Request $request) {
        return 'teste';
    })->name('teste');

    Route::get('/employees', [Controllers\EmployeeController::class, 'get'])
        ->name('employees.get');

    Route::get('/employees/{employee}', [Controllers\EmployeeController::class, 'show'])
        ->name('employees.show');

    Route::post('/employees', [Controllers\EmployeeController::class, 'storeOrUpdate'])
        ->name('employees.storeOrUpdate');

    Route::delete('/employees/{employee}', [Controllers\EmployeeController::class, 'destroy'])
        ->name('employees.destroy');
});
