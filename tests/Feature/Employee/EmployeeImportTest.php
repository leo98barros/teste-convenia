<?php

namespace Tests\Feature\Employee;

use App\Imports\EmployeeImport;
use App\Models\User;
use App\Notifications\EmployeeImportedNotification;
use App\Repositories\Employee\EmployeeRepositoryContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Queue;
use Laravel\Passport\Passport;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class EmployeeImportTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    private string $route;

    private EmployeeRepositoryContract $employeeRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->route = route('employees.storeOrUpdate');

        $this->user = User::factory()->create();
    }

    public function test_it_cannot_store_or_update_without_a_auth_user()
    {
        Passport::actingAsClient($this->user);

        $csvName = "employees_with_valid_data.csv";

        $file = UploadedFile::fake()->create($csvName);

        $this->postJson(route('employees.storeOrUpdate'), [ "file" => $file ])
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertExactJson([
                'message' => "Unauthenticated.",
            ]);
    }

    public function test_it_can_store_or_update_with_a_auth_user()
    {
        $this->actingAs($this->user);

        Excel::fake();

        $csvName = "employees_with_valid_data.csv";

        $file = UploadedFile::fake()->create($csvName);

        $this->postJson(route('employees.storeOrUpdate'), [ "file" => $file ])
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertExactJson([
                'message' => "Unauthenticated.",
            ]);
    }

    public function test_it_employee_was_imported_and_auth_user_was_notified()
    {
        Queue::fake();
        Excel::fake();
        Notification::fake();

        $csvName = base_path('tests/Feature/Employee/CSV/employees_with_valid_data.csv');

        $file = UploadedFile::fake()->create($csvName);

        $employeeRepository = $this->createMock(EmployeeRepositoryContract::class);

        (new EmployeeImport($employeeRepository, $this->user))->import($file->path());

        Excel::assertImported($file->path(), function(EmployeeImport $import) {
            $import->user->notify(new EmployeeImportedNotification('Notification mock'));

            return true;
        });

        Notification::assertSentTo(
            $this->user,
            EmployeeImportedNotification::class
        );
    }
}
